%global pkg_name %{name}

Name:       preupgrade-assistant-el6toel7-data
Version:    0.20200704
Release:    1%{?dist}
Summary:    Static data about differences between systems.

Group:      System Environment/Libraries
License:    GPLv3+
Source0:    %{name}-%{version}.tar.gz
#Source1:    LICENSE
BuildArch:  noarch
BuildRoot:  %{_tmppath}/%{pkg_name}-%{version}-%{release}-root-%(%{__id_u} -n)
# URL: missing - we are upstream and source URL is not available

# requires due to install path
Requires: preupgrade-assistant-el6toel7

%description
The package provides a set of static data about differences
between RHEL 6 and RHEL 7 systems

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p -m 755 $RPM_BUILD_ROOT%{_datadir}/doc/preupgrade-assistant-el6toel7-data
install -d -m 755 $RPM_BUILD_ROOT%{_datadir}/preupgrade/RHEL6_7/common
mv RHEL6_7/LICENSE $RPM_BUILD_ROOT%{_datadir}/doc/preupgrade-assistant-el6toel7-data/
mv RHEL6_7/* $RPM_BUILD_ROOT%{_datadir}/preupgrade/RHEL6_7/common

rm -rf $RPM_BUILD_ROOT%{_datadir}/preupgrade/RHEL5_*

find $RPM_BUILD_ROOT%{_datadir}/preupgrade/ -name "*.ignore" -delete
find $RPM_BUILD_ROOT%{_datadir}/preupgrade/ -name "*.log" -delete

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%dir %{_datadir}/doc/preupgrade-assistant-el6toel7-data
%doc %{_datadir}/doc/preupgrade-assistant-el6toel7-data/LICENSE
%dir %{_datadir}/preupgrade/RHEL6_7/common
%{_datadir}/preupgrade/RHEL6_7/common/*

%changelog
* Tue Jul 07 2020 Petr Stodulka <pstodulk@redhat.com> - 0.20200704-1
- Rebase to 0.20200704
  Resolves: #1854433

* Thu Feb 14 2019 Petr Stodulka <pstodulk@redhat.com> - 0.20190214-1
- Rebase to 0.20190214
  Resolves: #1677346

* Fri Oct 05 2018 Petr Stodulka <pstodulk@redhat.com> - 0.20181005-1
- Rebase to 0.20181005
  Resolves: #1613346

* Wed Aug 29 2018 Petr Stodulka <pstodulk@redhat.com> - 0.20180828-1
- Rebase to 0.20180828
- data for RHEL 6.10 -> 7.6
  Resolves: #1613346

* Fri Jun 01 2018 Petr Stodulka <pstodulk@redhat.com> - 0.20180601-1
- Rebase to 0.20180601
- data for RHEL 6.10 -> 7.5
  Resolves: #1585228

* Tue Apr 03 2018 Petr Stodulka <pstodulk@redhat.com> - 0.20180329-1
- Rebase to 0.20180329
- Related: #1546178

* Fri Feb 16 2018 Petr Stodulka <pstodulk@redhat.com> - 0.20180216-2
- rebuild

* Fri Feb 16 2018 Petr Stodulka <pstodulk@redhat.com> - 0.20180216-1
- Rebase to 0.20180216
- Resolves: #1546178

* Mon Jul 17 2017 Petr Stodulka <pstodulk@redhat.com> - 0.20170712-1
- rebase to 0.20170712
  Related: #14447727

* Wed Jun 21 2017 Petr Stodulka <pstodulk@redhat.com> - 0.20170620-1
- rebase to 0.20170620
  Related: #14447727

* Wed May 03 2017 Petr Stodulka <pstodulk@redhat.com> - 0.20170602-1
- rebase to 0.20170602
  Related: #14447727

* Wed May 03 2017 Petr Stodulka <pstodulk@redhat.com> - 0.20170426-1
- rebase to 0.20170426
- data for RHEL 6.9 -> 7.4
  Resolves: #1447727

* Sat Feb 18 2017 Petr Stodulka <pstodulk@redhat.com> - 0.20170218-1
- rebase to 0.20170218
- final data for RHEL 6.9 -> 7.3

* Thu Dec 01 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20161201-1
- rebase to 0.20161201
- data for RHEL 6.9 -> 7.3

* Thu Oct 13 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20161013-1
- rebase to 0.20161013

* Tue Sep 20 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.20160920-1
- rebase to 0.20160920

* Tue Aug 30 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20160830-1
- rebase to 0.20160830
- files BothMissing and Providesonlymissing contains only one line per
  each relevant old package with merged data from various repositories
  Resolves: #1371109

* Mon Aug 01 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20160730-1
- rebase to 0.20160730
- data for RHEL 6.8 -> 7.3

* Thu Jul 07 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20160707-1
- rebase to 0.20160707

* Thu May 05 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20160505-1
- rebase to 0.20160505
- fixed content of files BothMissing and Providesonlymissing

* Wed Apr 06 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20160406-1
- rebase to 0.20160406

* Fri Mar 11 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20160310-1
- rebase to 0.20160310
- fixed *grouplist* files names - now contains suffix always relevant
  to system release - e.g. el6, el7, ...
- moved LICENSE file to own doc directory

* Thu Mar 10 2016 Jakub Mazanek <jmazanek@redhat.com> - 0.20160309-1
- rebase 0.20160309

* Tue Feb 09 2016 Petr Stodulka <pstodulk@redhat.com> - 0.20160125-1
- initial git spec file

